# These templates have been reviewed by the debian-l10n-english
# team
#
# If modifications/additions/rewording are needed, please ask
# debian-l10n-english@lists.debian.org for advice.
#
# Even minor modifications require translation updates and such
# changes should be coordinated with translators and reviewers.

Template: minissdpd/why_I_am_here
Type: note
_Description: MiniSSDP daemon configuration
 The MiniSSDP daemon is being installed (perhaps as a dependency for UPnP
 support) but will not function correctly until it is configured.
 .
 MiniSSDP is a daemon used by MiniUPnPc to speed up device discovery. For
 security reasons, no out-of-box default configuration can be provided, so it
 requires manual configuration.
 .
 Alternatively you can simply override the recommendation and remove MiniSSDP,
 or leave it unconfigured - it won't work, but MiniUPnPc (and UPnP
 applications) will still function properly despite some performance loss.

Template: minissdpd/start_daemon
Type: boolean
Default: false
_Description: Start the MiniSSDP daemon automatically?
 Choose this option if the MiniSSDP daemon should start automatically,
 now and at boot time.

Template: minissdpd/listen
Type: string
_Description: Interfaces to listen on for UPnP queries:
 The MiniSSDP daemon will listen for requests on those interfaces, and drop
 all queries that do not come from the local network. Please enter the LAN
 interfaces that it should listen on, separated by space.

Template: minissdpd/ip6
Type: boolean
Default: false
_Description: Enable IPv6 listening?
 Please specify whether the MiniSSDP daemon should listen for IPv6 queries.
