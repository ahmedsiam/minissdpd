#!/bin/sh

set -e

DEFAULT_FILE=/etc/default/minissdpd

. /usr/share/debconf/confmodule

replace_config () {
	if [ -s "${1}" ]; then
		sed -ri '
x
s|^$||
t find
x
b
: find
x
s|^\s*('"${2}"'=).*|\1'"${3}"'|
t end
s|^#[ \t#]*('"${2}"'=['\'\"']?'"${3}"'['\'\"']?)\s*$|\1|
t end
s|^#[ \t#]*('"${2}"'=)\s*$|\1'"${3}"'|
t end
$ a'"${2}"'='"${3}"'
b
: end
h
		' "${1}"
	else
		echo "${2}=${3}" > "${1}"
	fi
}


#DEBHELPER#

case "${1}" in
	configure)
		# maintain the /etc/default/minissdpd configuration
		# only run on fresh installation / reconfiguration, #1033479
		if ! [ -e "${DEFAULT_FILE}" ] ; then
			cp /usr/share/minissdpd/minissdpd.default "${DEFAULT_FILE}"
			DEBCONF_RECONFIGURE=1
		fi

		if [ "${DEBCONF_RECONFIGURE}" = 1 ] ; then
			db_get minissdpd/listen
			MiniSSDPd_INTERFACE_ADDRESS="${RET}"
			replace_config "${DEFAULT_FILE}" MiniSSDPd_INTERFACE_ADDRESS "\"${MiniSSDPd_INTERFACE_ADDRESS}\""

			db_get minissdpd/ip6
			if [ "${RET}" = "true" ] ; then
				if ! echo "${MiniSSDPd_OTHER_OPTIONS}" | grep -qn '\-6' ; then
					MiniSSDPd_OTHER_OPTIONS="${MiniSSDPd_OTHER_OPTIONS} -6"
				fi
			else
				if echo "${MiniSSDPd_OTHER_OPTIONS}" | grep -qn '\-6' ; then
					MiniSSDPd_OTHER_OPTIONS=$(echo ${MiniSSDPd_OTHER_OPTIONS} | sed 's/-6//')
				fi
			fi
			MiniSSDPd_OTHER_OPTIONS=$(echo ${MiniSSDPd_OTHER_OPTIONS} | sed -r 's/^\s+//; s/\s+$//; s/  +/ /g')
			replace_config "${DEFAULT_FILE}" MiniSSDPd_OTHER_OPTIONS "\"${MiniSSDPd_OTHER_OPTIONS}\""

			db_get minissdpd/start_daemon
			if [ "${RET}" = "true" ] ; then
				update-rc.d minissdpd enable > /dev/null
				if [ -n "$2" ]; then
					_dh_action=restart
				else
					_dh_action=start
				fi
				invoke-rc.d minissdpd $_dh_action
				# if false, don't disable it - no-enable on first installation and keep everything untouched during upgrade
			fi

			db_stop
		fi
	;;

	abort-upgrade|abort-remove|abort-deconfigure)
	;;

	*)
		echo "postinst called with unknown argument \`${1}'" >&2
		exit 1
	;;
esac

exit 0
