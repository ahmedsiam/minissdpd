minissdpd (1.6.0-2) unstable; urgency=medium

  * Preserve user's modification on default file (Closes: #1033479).
  * Move systemd wrapper into /usr/libexec.
  * No longer hint IP addresses of interfaces in debconf.
  * Add Romanian Debconf translations, thanks to Remus-Gabriel Chelu
    (Closes: #1032721).

 -- Yangfl <mmyangfl@gmail.com>  Wed, 07 Feb 2024 22:06:07 +0800

minissdpd (1.6.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.6.2.

 -- Yangfl <mmyangfl@gmail.com>  Thu, 22 Dec 2022 01:07:49 +0800

minissdpd (1.5.20211105-1) unstable; urgency=medium

  * New upstream release.
  * Do not fail when service fails to start (Closes: #969184).
  * Add Spanish Debconf translations, thanks to Camaleón (Closes: #987340).
  * Bump Standards-Version to 4.6.0.

 -- Yangfl <mmyangfl@gmail.com>  Mon, 13 Dec 2021 14:44:44 +0800

minissdpd (1.5.20190824-1) unstable; urgency=medium

  * New upstream release.
  * Fixed annoying startup message "... unexpected operator" when
    MiniSSDPd_INTERFACE_ADDRESS contains more then one interface
    (Closes: #924865).
  * Start systemd service before miniupnpd (Closes: #961695).
  * Update Debconf translations, with thanks to:
    - French, Jean-Pierre Giraud (Closes: #924079).
    - Russian, Lev Lamberov (Closes: #951433).
  * Skip starting service when unconfigured (Closes: #932138).
  * Bump Standards-Version to 4.5.0.
  * Bump debhelper compat to 13.
  * Add upstream metadata.

 -- Yangfl <mmyangfl@gmail.com>  Tue, 25 Aug 2020 01:04:06 +0800

minissdpd (1.5.20190210-1) unstable; urgency=medium

  * New upstream release.
  * Add/update Debconf translations, with thanks to:
    - Swedish, Martin Bagge / brother (Closes: #922139).
    - German, Helge Kreutzmann (Closes: #922147).
    - Portuguese, Américo Monteiro (Closes: #922512).
    - Dutch, Frans Spiesschaert (Closes: #922971).
    - Brazilian Portuguese, Adriano Rafael Gomes (Closes: #923004).
    - Danish, Joe Dalton (Closes: #923054).
  * debian/watch: Check for PGP signature.

 -- Yangfl <mmyangfl@gmail.com>  Thu, 28 Feb 2019 11:13:55 +0800

minissdpd (1.5.20180223-6) unstable; urgency=medium

  * debian/minissdpd.config: do not use /sbin/ifconfig if it's not available.
    Fallback to reading /proc/net/route if we can read it. (Closes: #921736).

 -- Thomas Goirand <zigo@debian.org>  Tue, 12 Feb 2019 11:19:29 +0100

minissdpd (1.5.20180223-5) unstable; urgency=medium

  * Update debconf questions (Closes: #908586).
  * Guess some interfaces for listerning.
  * Add/update Debconf translations, with thanks to:
    - Danish, Joe Dalton (Closes: #919080).
    - Portuguese, Américo Monteiro (Closes: #919215).
    - German, Helge Kreutzmann (Closes: #919425).
    - Dutch, Frans Spiesschaert (Closes: #920428).
    - Russian, Lev Lamberov (Closes: #920981).
    - Vietnamese, Trần Ngọc Quân.
  * Fix autopkgtest.

 -- Yangfl <mmyangfl@gmail.com>  Tue, 08 Jan 2019 15:17:35 +0800

minissdpd (1.5.20180223-4) unstable; urgency=medium

  * Add a upgrade-only debconf note (Closes: #916699).
  * Remove START_DAEMON in /etc/default/miniupnpd (Closes: #908585).
  * Support specifying multiple interface names (Closes: #908586).
  * Add Portuguese Debconf translations, thanks to Américo Monteiro
    (Closes: #910399).
  * Add autopkgtest.
  * Bump Standards-Version to 4.3.0.
  * Bump debhelper compat to 12.

 -- Yangfl <mmyangfl@gmail.com>  Mon, 07 Jan 2019 00:01:42 +0800

minissdpd (1.5.20180223-3) unstable; urgency=medium

  * debian/minissdpd.config: Always prefer configs in debconf to prevent config
    loss during fresh installation (Closes: #901605, #903783).
  * debian/rules: Disable auto enabling systemd service (Closes: #901658).
  * Add Debconf translations, with thanks to:
    - Brazilian Portuguese, Adriano Rafael Gomes (Closes: #903357).
    - German, Helge Kreutzmann (Closes: #905758).
  * Bump Standards-Version to 4.2.0.

 -- Yangfl <mmyangfl@gmail.com>  Thu, 26 Jul 2018 20:39:35 +0800

minissdpd (1.5.20180223-2) unstable; urgency=medium

  * Make init script more permissive (Closes: #898008).
  * Add Debconf translations, with thanks to:
    - Russian, Lev Lamberov (Closes: #898183).
    - Dutch, Frans Spiesschaert (Closes: #898868).
    - French, jean-pierre giraud (Closes: #899297).
  * Fix sed error caused by "/" in CIDR address (Closes: #900169).
  * Hotfix FTBFS on kfreebsd-*.
  * Make Homepage field more accurate.

 -- Yangfl <mmyangfl@gmail.com>  Mon, 11 Jun 2018 20:16:49 +0800

minissdpd (1.5.20180223-1) unstable; urgency=medium

  * New upstream release.
    - Update manpage (Closes: #889143).
    - Use receiving interface index to check if from LAN (Closes: #896009).
  * Disable hurd-any due to lack of rt_msghdr and RTM_*.
  * Use debconf to ask for listerning interface (Closes: #890584).
  * Bump Standards-Version to 4.1.4.

 -- Yangfl <mmyangfl@gmail.com>  Sat, 21 Apr 2018 16:50:21 +0800

minissdpd (1.5.20161216-2) unstable; urgency=medium

  * Fix missing /etc/default/minissdpd (Closes: #889028)
  * Allow specifying IPv4 addresses (Closes: #889138)
  * Temporarily fix FTBFS on Hurd
  * Enable pristine-tar in debian/gbp.conf

 -- Yangfl <mmyangfl@gmail.com>  Fri, 02 Feb 2018 00:54:30 +0800

minissdpd (1.5.20161216-1) unstable; urgency=medium

  [ Yangfl ]
  * New upstream release (Closes: #852792).
  * Enable IPv6 support by default (Closes: #658256).
  * Use /bin/sh in initscript (Closes: #762937).
  * Fix FTCBFS by Helmut Grohne <helmut@subdivi.de> (Closes: #839117).
    - Pass triplet-prefixed CC to make
    - Fix build/host confusion

  [ Thomas Goirand ]
  * Update VCS fields to point to Salsa.
  * Removed bsdqueue.h from debian/copyright (gone upstream...).
  * Update copyright years.
  * Fixed debian/gbp.conf.

 -- Thomas Goirand <zigo@debian.org>  Thu, 01 Feb 2018 09:31:25 +0000

minissdpd (1.2.20130907-4) unstable; urgency=medium

  * Add After=network-online.target in the .service file (Closes: #861231).
  * Add lsb-base as depends.

 -- Thomas Goirand <zigo@debian.org>  Wed, 26 Apr 2017 17:07:25 +0200

minissdpd (1.2.20130907-3.2) unstable; urgency=high

  * Non-maintainer upload.
  * Fix CVE-2016-3178 and CVE-2016-3179. (Closes: #816759)
    The minissdpd daemon contains a improper validation of array index
    vulnerability (CWE-129) when processing requests sent to the Unix
    socket at /var/run/minissdpd.sock the Unix socket can be accessed
    by an unprivileged user to send invalid request causes an
    out-of-bounds memory access that crashes the minissdpd daemon.

 -- James Cowgill <jcowgill@debian.org>  Mon, 24 Oct 2016 08:54:59 +0100

minissdpd (1.2.20130907-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add systemd service file (Closes: #716803).

 -- Michael Biebl <biebl@debian.org>  Wed, 13 Jul 2016 20:12:37 +0200

minissdpd (1.2.20130907-3) unstable; urgency=medium

  * Removed $all from init.d script.
  * Removed build-depends on hardening-wrapper.

 -- Thomas Goirand <zigo@debian.org>  Mon, 14 Jul 2014 14:48:55 +0800

minissdpd (1.2.20130907-2) unstable; urgency=medium

  * Build-Depends: on freebsd-glue, and link with it, if we're building for
    kfreebsd. This is needed for the link_ntoa() call in upnputils.c, as
    otherwise minissdpd FTBFS on kfreebsd.

 -- Thomas Goirand <zigo@debian.org>  Mon, 09 Jun 2014 14:34:14 +0800

minissdpd (1.2.20130907-1) unstable; urgency=medium

  * New upstream release (Closes: #719612).
  * Fixed typo in package description (Closes: #653027).
  * Removed 0001-always-disable-link_ntoa.diff, let's see if that still
    works in FreeBSD.
  * Switched to compat level 9 and bumped standards-version.
  * VCS URLs now canonical.
  * Using DPKG_EXPORT_BUILDFLAGS and hardening=+all.
  * Removed the [ $VERBOSE ] cruft from init script.

 -- Thomas Goirand <zigo@debian.org>  Wed, 28 May 2014 06:47:51 +0000

minissdpd (1.1.20120121-1) unstable; urgency=low

  * New upstream version.
  * Fixed the init script to handle when start-stop-daemon returns 1, and
  handling of the VERBOSE variable.
  * Switching from dpatch to quilt, and from source format 1.0 to 3.0.

 -- Thomas Goirand <zigo@debian.org>  Fri, 17 Feb 2012 19:23:48 +0800

minissdpd (1.1.20111007-4) unstable; urgency=low

  * Fixed English grammar mistakes, thanks to Martin Eberhard Schauer
  <Martin.E.Schauer@gmx.de> (Closes: #653027).
  * Added a debian/gbp.conf
  * Added support for "status" action to init.d script, thanks to patch from
  Peter Eisentraut <petere@debian.org> (Closes: #652916).

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Dec 2011 19:53:11 +0800

minissdpd (1.1.20111007-3) unstable; urgency=low

  * Exits if binary isn't found (Closes: #646746).

 -- Thomas Goirand <zigo@debian.org>  Thu, 27 Oct 2011 19:28:20 +0800

minissdpd (1.1.20111007-2) unstable; urgency=low

  * Calling dpatch directly in debian/rules because the patch wasn't applied at
  all, so this bug was still remaining, thanks to Christoph Egger
  <christoph@debian.org> for reporting it (Closes: #635911).

 -- Thomas Goirand <zigo@debian.org>  Mon, 10 Oct 2011 15:16:20 +0800

minissdpd (1.1.20111007-1) unstable; urgency=low

  * New upstream version including the bugfixes to bugs sent to the Debian
  BTS (Closes: #644508, #644509, #644510, #644511, #630665).
  * Added a patch to disable calls to link_ntoa in Debian/kFreeBSD
  (Closes: #635911).
  * debian/copyright is now in DEP5 format.

 -- Thomas Goirand <zigo@debian.org>  Sun, 09 Oct 2011 17:49:12 +0000

minissdpd (1.0.20110729-1) unstable; urgency=high

  * New upstream release 1.0.20110729, fixing root exploit issue reported on
  launchpad (Closes: #635836) (LP: #813313), thanks to Moritz Muehlenhoff
  <jmm@debian.org> for the bug report, and to falks at Ubuntu for the
  investigation of the issue.
  * Added build-arch: and build-indep: targets in debian/rules.
  * Bumped standard-version to 3.9.2.

 -- Thomas Goirand <zigo@debian.org>  Fri, 29 Jul 2011 14:41:55 +0200

minissdpd (1.0-2) unstable; urgency=low

  * Watch file was wrong (it was the one of miniupnpc).
  * Uploading to unstable from now on.

 -- Thomas Goirand <zigo@debian.org>  Fri, 11 Mar 2011 19:59:51 +0800

minissdpd (1.0-1) experimental; urgency=low

  * Initial release (Closes: #608270).

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Dec 2010 16:49:20 +0800
