#!/bin/sh

set -e

DEFAULT_FILE=/etc/default/minissdpd

. /usr/share/debconf/confmodule


# fetch MiniSSDPd_* vars

[ -r "${DEFAULT_FILE}" ] && . "${DEFAULT_FILE}"

# fetch config from debconf

db_get minissdpd/start_daemon
if [ "${RET}" = true ] ; then
	START_DAEMON=1
else
	START_DAEMON=0
fi

# do not override user's modification on default file
if [ -z "${MiniSSDPd_INTERFACE_ADDRESS}" ] ; then
	db_get minissdpd/listen
	[ -n "${RET}" ] && MiniSSDPd_INTERFACE_ADDRESS="${RET}"
fi

[ "${MiniSSDPd_INTERFACE_ADDRESS}" = "0.0.0.0" ] && MiniSSDPd_INTERFACE_ADDRESS=

# if no interface is defined, try to find some automatically
if [ -z "${MiniSSDPd_INTERFACE_ADDRESS}" ] ; then
	if which ip > /dev/null ; then
		interfaces=$(LC_ALL=C ip l | grep 'MULTICAST,UP' | cut -d: -f2 | tr '\n' ' ')
	elif which ifconfig > /dev/null ; then
		interfaces=$(LC_ALL=C ifconfig | grep 'UP,BROADCAST' | cut -d: -f1 | tr '\n' ' ')
	elif [ -r /proc/net/route ] && which awk > /dev/null ; then
		interfaces=$(awk '{ print $1 }' /proc/net/route | grep -v Iface | sort -u | tr '\n' ' ')
	fi
	[ -n "${interfaces}" ] && MiniSSDPd_INTERFACE_ADDRESS=$(echo "${interfaces}" | sed -r 's/^\s+//; s/\s+$//; s/  +/ /g')
fi

db_set minissdpd/listen "${MiniSSDPd_INTERFACE_ADDRESS}"

# enable it if fresh installation
[ -z "$2" ] && [ -n "${MiniSSDPd_INTERFACE_ADDRESS}" ] && START_DAEMON=1

if [ "${START_DAEMON}" = 1 ] ; then
	db_set minissdpd/start_daemon true
else
	db_set minissdpd/start_daemon false
fi

# ask user inputs

if [ -n "$2" ] && dpkg --compare-versions "$2" lt 1.5.20180223-5 ; then
	if [ -z "${MiniSSDPd_INTERFACE_ADDRESS}" ] ; then
		db_input high minissdpd/why_I_am_here || true
	fi
fi
db_input high minissdpd/start_daemon || true
db_input high minissdpd/listen || true
db_input low minissdpd/ip6 || true
db_go

exit 0
