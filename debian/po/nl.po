# Dutch translation of minissdpd debconf templates.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the minissdpd package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# Frans Spiesschaert <Frans.Spiesschaert@yucom.be>, 2018, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: minissdpd 1.5.20180223-6\n"
"Report-Msgid-Bugs-To: minissdpd@packages.debian.org\n"
"POT-Creation-Date: 2024-02-08 14:45+0800\n"
"PO-Revision-Date: 2019-02-18 22:15+0100\n"
"Last-Translator: Frans Spiesschaert <Frans.Spiesschaert@yucom.be>\n"
"Language-Team: Debian Dutch l10n Team <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Gtranslator 2.91.7\n"

#. Type: note
#. Description
#: ../minissdpd.templates:2001
msgid "MiniSSDP daemon configuration"
msgstr "Configuratie van de achtergronddienst MiniSSDP"

#. Type: note
#. Description
#: ../minissdpd.templates:2001
msgid ""
"The MiniSSDP daemon is being installed (perhaps as a dependency for UPnP "
"support) but will not function correctly until it is configured."
msgstr ""
"De achtergronddienst MiniSSDP werd geïnstalleerd (misschien als een vereiste "
"voor UPnP-ondersteuning), maar zal niet correct functioneren totdat deze "
"geconfigureerd werd."

#. Type: note
#. Description
#: ../minissdpd.templates:2001
msgid ""
"MiniSSDP is a daemon used by MiniUPnPc to speed up device discovery. For "
"security reasons, no out-of-box default configuration can be provided, so it "
"requires manual configuration."
msgstr ""
"MiniSSDP is een achtergronddienst die door MiniUPnPc gebruikt wordt om "
"apparaten sneller te vinden. Uit veiligheidsoverwegingen kan niet meer in "
"een automatische standaardconfiguratie voorzien worden. Daarom is een "
"manuele configuratie vereist."

#. Type: note
#. Description
#: ../minissdpd.templates:2001
msgid ""
"Alternatively you can simply override the recommendation and remove "
"MiniSSDP, or leave it unconfigured - it won't work, but MiniUPnPc (and UPnP "
"applications) will still function properly despite some performance loss."
msgstr ""
"Een andere mogelijkheid is om de aanbeveling gewoon te omzeilen en MiniSSDP "
"te verwijderen of het niet-geconfigureerd te laten - het zal niet werken, "
"maar MiniUPnPc (en UPnP-toepassingen) zullen nog steeds behoorlijk "
"functioneren (ondanks enig prestatieverlies)."

#. Type: boolean
#. Description
#: ../minissdpd.templates:3001
msgid "Start the MiniSSDP daemon automatically?"
msgstr "De achtergronddienst MiniSSDP automatisch starten?"

#. Type: boolean
#. Description
#: ../minissdpd.templates:3001
msgid ""
"Choose this option if the MiniSSDP daemon should start automatically, now "
"and at boot time."
msgstr ""
"Kies deze optie als de achtergronddienst MiniSSDP nu en bij het starten van "
"de computer automatisch moet opstarten."

#. Type: string
#. Description
#: ../minissdpd.templates:4001
msgid "Interfaces to listen on for UPnP queries:"
msgstr "Interfaces waarop naar UPnP-aanvragen geluisterd moet worden:"

#. Type: string
#. Description
#: ../minissdpd.templates:4001
#, fuzzy
#| msgid ""
#| "The MiniSSDP daemon will listen for requests on one interface, and drop "
#| "all queries that do not come from the local network. Please enter the LAN "
#| "interfaces or IP addresses of those interfaces (in CIDR notation) that it "
#| "should listen on, separated by space."
msgid ""
"The MiniSSDP daemon will listen for requests on those interfaces, and drop "
"all queries that do not come from the local network. Please enter the LAN "
"interfaces that it should listen on, separated by space."
msgstr ""
"De achtergronddienst MiniSSDP zal aanvragen beluisteren op één interface en "
"alle verzoeken die niet van het lokale netwerk afkomstig zijn, laten vallen. "
"Vermeld de LAN-interfaces of de IP-adressen van deze interfaces (in CIDR-"
"notatie) waarop geluisterd moet worden, van elkaar gescheiden met een spatie."

#. Type: boolean
#. Description
#: ../minissdpd.templates:5001
msgid "Enable IPv6 listening?"
msgstr "Beluisteren van IPv6 activeren?"

#. Type: boolean
#. Description
#: ../minissdpd.templates:5001
msgid ""
"Please specify whether the MiniSSDP daemon should listen for IPv6 queries."
msgstr ""
"Geef aan of de achtergronddienst MiniSSDP IPv6-aanvragen moet beluisteren."

#~ msgid ""
#~ "Interface names are highly preferred, and required if you plan to enable "
#~ "IPv6 port forwarding."
#~ msgstr ""
#~ "De interfacenamen genieten een sterke voorkeur en deze zijn zelfs vereist "
#~ "als u zinnens bent IPv6 port forwarding te gebruiken."

#~ msgid ""
#~ "If you see this message but never manually install MiniSSDP, then most "
#~ "probably this package is automatically pulled as a recommendation for "
#~ "libminiupnpc, which is the UPnP support library for many applications."
#~ msgstr ""
#~ "Indien u deze boodschap leest, maar MiniSSDP nooit zelf geïnstalleerd "
#~ "heeft, dan werd dit pakket waarschijnlijk automatisch geïnstalleerd omdat "
#~ "het aanbevolen wordt door libminiupnpc. Dit laatste is een bibliotheek "
#~ "die UPnP-ondersteuning biedt voor veel toepassingen."
